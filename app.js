import axios from 'axios'
import PromiseQueue from 'promise-queue'
import recognizeDate from './recognizeDate'

function voice(text) {
    return new Promise(function (resolve, reject) {
        var voice = new Audio('./test.php?text=' + text);
        voice.onended = resolve;
        return voice.play();
    });
}

window.speech = function() {
    return new Promise(function (resolve, reject) {
        var recognizer = new webkitSpeechRecognition();
        var textarea1 = document.getElementById("text");

        recognizer.interimResults = true;

        recognizer.lang = 'ru-RU';

        recognizer.onresult = function (event) {
            console.log(event);
            var result = event.results[event.resultIndex];
            if (result.isFinal) {
                resolve(result[0].transcript.toLowerCase());
                console.log(result[0].transcript);
                // textarea1.innerHTML = 'Вы сказали: ' + result[0].transcript;
                // var way = undersanding(result[0].transcript);
            } else {
                textarea1.innerHTML = 'Промежуточный резльутат: ' + result[0].transcript;
            }
        };

        recognizer.start();
    });
};

window.understanding = function(text) {
    console.log(text);
    var actionwords1 = ["постав", "установ", "внес", "запиш", "добав"];
    var actionwords2 = ["напомн", "запомн"];
    var actionwords3 = ["запланир", "запиш"];
    var typetaskw = ["задач", "дел"];
    var typerememberw = ["напомина", "напомни", "задач", "дел"];
    var typemeetw = ["встреч", "собран", "совещан", "переговор"];
    var type_array = ["задача", "напоминание", "встреча"];
    var geo = ["москв", "народного ополчен", "питер"];
    var person = ["макс", "петр", "лен", "майк"];
    var subject = ["помыть посуду", "покормить кошку", "купить молоко"];
    var combinations = [
        {
            type: 'task',
            combinations: [{
                noun: typetaskw,
                verb: actionwords1
            }]
        },
        {
            type: 'notification',
            combinations: [{
                noun: typerememberw,
                verb: actionwords2
            }]
        },
        {
            type: 'appointment',
            combinations: [{
                noun: typemeetw,
                verb: actionwords3
            }]
        }
    ];

    var task = {
        type: "",
        subject: null,
        time: "",
        geo: "",
        person: ""
    };

    var taskVariant = combinations.find(item => {
        return item.combinations.some(function (value) {
            var noun = value.noun.some(function (word) {
                return text.includes(word);
            });
            var verb = value.verb.some(function (word) {
                return text.includes(word);
            });
            return noun && verb;
        });
    });
    if (taskVariant) {
        task.type = taskVariant.type;
    }
    task.geo = geo.find(item => {
        return text.includes(item);
    });
    task.person = person.find(item => {
        return text.includes(item);
    });
    task.subject = subject.find(item => {
        return text.includes(item);
    });
    task.time = recognizeDate(text);
    console.log(task);
    var promises = [];
    var queue = new PromiseQueue(1, Infinity);
    if(!task.subject){
        promises.push(queue.add(function () {
            return voice("Что будем делать?").then(speech).then(function (text) {
                task.subject = text;
                console.log(task);
            });
        }))
    }
    if(!task.time && task.type === 'notification'){
        promises.push(queue.add(function () {
            return voice("Во сколько напомнить?").then(speech).then(function (text) {
                task.time = text;
                console.log(task);
            });
        }))
    }
    if(!task.geo && task.type === 'appointment'){
        promises.push(queue.add(function () {
            return voice("Где будет встреча?").then(speech).then(function (text) {
                task.geo = text;
                console.log(task);
            });
        }))
    }
    Promise.all(promises).then(function () {
        console.log(task);
        console.log("Все готово!");
    });
}