<?php

$text = $_GET['text'];

$postdata = http_build_query(
	array(
		'text' => $text,
		'lang' => 'ru-RU',
		'emotion' => 'neutral',
		'voice' => 'ermil',
		'folderId' => 'b1gbb4rpunbcoqinvsgj'
	)
);

$opts = array('http' =>
	array(
		'method'  => 'POST',
		'header'  => 'Authorization: Bearer CggaATEVAgAAABKABEPaBZJz-keAlMsvFS_aS3-A-h597VLOn1kO3tRfLXXjcUwE2iyfWAi4tXfjd0vwEn-EM2S3XW88jspiyvbILU--tvZA8fzefwLffyIj3I5tG417d1jp_k04W_hD5nScwSYK1ZcYZmagqZmdJ12xe8P2uV6ri6RwPn9cd614Db2YrNwUm8YkRmiRbo9gwKOdg8gfKSlOI5ztLubw_nyA95b_fwhfXSj2EmQqaDQy16GOIqqzO4a3KS7JZrk1zZGW54nQcC9qJCmyLFeJVOjpsqLwXmIUYmPm3ZfoxBBclWjqCtPqHnqPEU4Rxg01E2mjlmGiisIRRpCix2XpP4JxlW6J4SMhi9KpGPbvTcBKTLSJvUyUJoESIspKrwOnWRWCpWlrup-2QE7QxAtiEXGliUelMgNZ9YjPaVbAEejy5bg7WRZfnRNILjA-YHFWyOjVPbeSN7h9luo5vH9DdEWryKgq8bsJhdwFXBEjSP5tLQfHc4mKL9CGk3pjisvwwvymhoUf83vc-V3zaaR5ECziOYigLZ9LP9sviQWfx_2ruxDD9g27rUKAIKZaLWjxguOtbvGx8wwdV992xQvRnWCy0rr-bAqDO_rq_Tlhe66wDQ4qQ4NUqEI1XIFcZ9P1xTTUyP9cZZj8kcbvOHlO4c0XZ6GB9XFVfXix706ycyFpLyPBGl8KIDc1MWRhNTZmMjcwOTQ0NWVhZmM5OTM3N2JjM2I0NDE0EK-SseIFGO_js-IFIh0KFGFqZXMxYjdic20yOXRpZWE2OW9pEgVzaHVpa1oAMAI4AUoIGgExFQIAAABQASDuBA',
		'content' => $postdata
	)
);

$context  = stream_context_create($opts);

$result = file_get_contents('https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize', false, $context);
header('Content-type: audio/ogg');
echo $result;